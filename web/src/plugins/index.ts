/**
 * plugins/index.ts
 *
 * Automatically included in `./src/main.ts`
 */

// Plugins
import vuetify from './vuetify'
import pinia from '../stores'
import router from '../router'
import authRouter from '../router/auth'
import http from "../plugins/axios"

// Types
import type { App } from 'vue'

export function registerPlugins (app: App) {
  app
    .use(vuetify)
    .use(router)
    .use(pinia)
    .use(http, {baseURL: '/'})
}

export function registerAuthPlugins (app: App) {
  app
    .use(vuetify)
    .use(authRouter)
    .use(pinia)
    .use(http, {baseURL: '/'})
}
