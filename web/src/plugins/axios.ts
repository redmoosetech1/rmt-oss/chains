import axios, {AxiosError, AxiosResponse, AxiosInstance} from 'axios'
import type {App} from 'vue'


interface AxiosOptions {
  baseURL?: string,
  token?: string,
}

export default {
  install: (app: App, options: AxiosOptions) => {
    const http: AxiosInstance = axios.create({
      baseURL: options.baseURL,
      xsrfCookieName: 'csrftoken',
      xsrfHeaderName: "X-CSRFTOKEN",
      withCredentials: true,
      withXSRFToken: true,
    })
    http.interceptors.response.use((config) => {
      console.log(config)
      return config
    })
    app.config.globalProperties.$axios = http
    app.provide('http', http)
  }
}
