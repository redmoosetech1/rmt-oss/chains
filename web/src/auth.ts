/**
 * auth.ts
 *
 * Bootstraps Vuetify and other plugins then mounts the Auth`
 */

// Plugins
import { registerAuthPlugins } from './plugins'

// Components
import Auth from './Auth.vue'

// Composable
import { createApp } from 'vue'

const app = createApp(Auth)

registerAuthPlugins(app)

app.mount('#auth')
