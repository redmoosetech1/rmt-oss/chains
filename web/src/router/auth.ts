/**
 * router/auth.ts
 */

// Composables
import { createRouter, createWebHistory } from 'vue-router'
import AuthLayout from '../layouts/AuthLayout.vue'
import LoginView from '../views/LoginView.vue'
import RegisterView from '../views/RegisterView.vue'
import ForgotPasswordView from '../views/ForgotPasswordView.vue'
import ResetPasswordView from '../views/ResetPasswordView.vue'

const routes = [
  {
    path: '/accounts',
    component: AuthLayout,
    children: [
      {
        path: '/login',
        component: LoginView,
      },
      {
        path: '/register',
        component: RegisterView,
      },
      {
        path: '/forgot-password',
        component: ForgotPasswordView,
      },
      {
        path: '/reset-password',
        component: ResetPasswordView,
      }
    ]
  }
]

const router = createRouter({
  history: createWebHistory('/accounts/'),
  routes,
})

export default router
