/**
 * router/index.ts
 *
 * Automatic routes for `./src/pages/*.vue`
 */

// Composables
import { createRouter, createWebHistory } from 'vue-router'
import AppLayout from "../layouts/AppLayout.vue";
import AuthoritiesView from "../views/AuthoritiesView.vue";

const routes = [
  {
    path: '/',
    component: AppLayout,
    children: [
      {
        path: '/authorities',
        component: AuthoritiesView,
      }
    ]
  }
]

const router = createRouter({
  history: createWebHistory('/'),
  routes
})

export default router
