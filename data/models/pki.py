from django.db import models
import uuid


class PrivateKey(models.Model):
    """
    Private Key
    """
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    key = models.BinaryField()
    key_type = models.IntegerField()
    key_size = models.IntegerField()
    exponent = models.IntegerField()

    class Meta:
        db_table = "private_key"


class SubjectAlternativeName(models.Model):
    """
    Subject alternate name
    """
    name = models.CharField(max_length=260)

    class Meta:
        db_table = "subject_alternative_name"


class Certificate(models.Model):
    """
    Certificate
    """
    uuid = models.UUIDField(auto_created=uuid.uuid4, primary_key=True, default=uuid.uuid4, editable=False)
    common_name = models.CharField(max_length=255)
    signature_algorithm = models.CharField(max_length=255)
    issuer_name = models.CharField(max_length=255)
    serial_number = models.CharField(max_length=255, unique=True, null=True)
    key_usage = models.BinaryField(null=True)
    extended_key_usage = models.BinaryField(null=True)
    basic_constraint = models.BinaryField(null=True)
    not_valid_before = models.DateTimeField()
    not_valid_after = models.DateTimeField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    deleted = models.BooleanField(default=False)
    deleted_at = models.DateTimeField(null=True)
    subject_alternative_names = models.ManyToManyField(SubjectAlternativeName)
    private_key = models.ForeignKey(PrivateKey, on_delete=models.CASCADE)

    class Meta:
        db_table = "certificate"

    def __str__(self):
        return self.common_name
