from django.conf import settings
from django.http import HttpRequest


def context_debug(_: HttpRequest) -> dict:
    return {
        "DEBUG": settings.DEBUG,
    }
