from cryptography import x509
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.x509.oid import NameOID
from datetime import datetime, timedelta
from cryptography.hazmat.primitives.serialization import Encoding, PrivateFormat, NoEncryption


class PrivateKey:
    """
    Private Key Object
    """

    def __init__(self, identifier: str, key_size: int, public_exponent: int = 65537):
        self.identifier = identifier
        self.key_size = key_size
        self.public_exponent = public_exponent

    def create(self) -> bytes:
        """
        :return:
        """
        private_key = rsa.generate_private_key(
            key_size=self.key_size,
            public_exponent=self.public_exponent,
        )
        return private_key.private_bytes(
            Encoding.PEM,
            PrivateFormat.TraditionalOpenSSL,
            NoEncryption()
        )

    def __str__(self):
        return self.identifier
