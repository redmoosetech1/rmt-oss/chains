from api.serializers import LoginSerializer

Response_T = tuple[dict, dict]


def validate_login(data: dict) -> Response_T:
    serializer = LoginSerializer(data=data)
    if not serializer.is_valid():
        return {}, serializer.errors
    return serializer.data, {}
