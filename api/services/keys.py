from common.pki import PrivateKey
from api.serializers import PrivateKeySerializer


def create_private_key(identity: str, key_size: int, public_exponent):
    """
    :param identity:
    :param key_size:
    :param public_exponent:
    :return:
    """
    # 1. create private
    private_key = PrivateKey(identity, key_size, public_exponent)
    pk = private_key.create()

