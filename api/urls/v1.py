from typing import Callable
from rest_framework.routers import SimpleRouter
from api.views import AccountsView

router = SimpleRouter(trailing_slash=False)
# Accounts
# BaseAccountsPath: Callable[[str], str] = lambda x: f'v1/accounts/{x}'
router.register('v1/accounts', AccountsView, basename='api_accounts')
