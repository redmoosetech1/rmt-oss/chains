from data.models import PrivateKey
from rest_framework import serializers


class LoginSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField()


class PrivateKeySerializer(serializers.Serializer):
    """
    Private Key Serializer
    """
    class Meta:
        model = PrivateKey
        fields = '__all__'
