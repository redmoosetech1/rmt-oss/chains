from django.conf import settings
from django.http import HttpResponseRedirect
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.views import APIView
from rest_framework.viewsets import ViewSet
from rest_framework.response import Response
from rest_framework import status
from django.contrib.auth import login, logout, authenticate
from api.services import accounts


class AccountsView(ViewSet):
    authentication_classes = (SessionAuthentication, BasicAuthentication)

    def get_permissions(self, *args, **kwargs) -> list:
        print(self.request.META)
        if self.action in ['login']:
            return [AllowAny(),]
        return [IsAuthenticated(),]

    @action(methods=['POST'], detail=False, url_path='login', url_name='login')
    def login(self, request):
        data, error = accounts.validate_login(request.data)
        if error:
            return Response(error, status.HTTP_400_BAD_REQUEST)

        if user := authenticate(username=data.get('username'), password=data.get('password')):
            if user.is_active:
                login(request, user)
                return Response(status=status.HTTP_200_OK)
        return Response(status=status.HTTP_404_NOT_FOUND)

    @action(methods=['GET'], detail=False, url_path='logout', url_name='logout')
    def logout(self, request):
        if request.user.is_authenticated and request.user.is_active:
            logout(request)
            return Response(status=status.HTTP_200_OK)
        return Response(status=status.HTTP_404_NOT_FOUND)
