from cryptography import x509
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.x509.oid import NameOID
from datetime import datetime, timedelta
from cryptography.hazmat.primitives.serialization import Encoding, PrivateFormat, NoEncryption


def create_ca_certificate():
    # Generate a private key for the CA
    private_key = rsa.generate_private_key(
        public_exponent=65537,
        key_size=2048
    )

    # Create a certificate for the CA
    ca_cert = x509.CertificateBuilder().subject_name(
        x509.Name([
            x509.NameAttribute(NameOID.COMMON_NAME, u'My CA'),
        ])
    ).issuer_name(
        x509.Name([
            x509.NameAttribute(NameOID.COMMON_NAME, u'My CA'),
        ])
    ).not_valid_before(
        datetime.utcnow()
    ).not_valid_after(
        datetime.utcnow() + timedelta(days=3650)  # Valid for 10 years
    ).serial_number(
        x509.random_serial_number()
    ).public_key(
        private_key.public_key()
    ).add_extension(
        x509.BasicConstraints(ca=True, path_length=None), critical=True,
    ).sign(private_key, hashes.SHA256())

    # Save the private key and CA certificate to files
    with open("ca_private_key.pem", "wb") as key_file:
        key_file.write(
            private_key.private_bytes(
                Encoding.PEM, PrivateFormat.TraditionalOpenSSL, NoEncryption()
            )
        )

    with open("ca_certificate.pem", "wb") as cert_file:
        cert_file.write(ca_cert.public_bytes(Encoding.PEM))


if __name__ == "__main__":
    create_ca_certificate()
