import paramiko
from paramiko import SSHException
import os

def create_ssh_ca(ca_key_path, ca_cert_path):
    # Generate an SSH key pair for the CA if they don't already exist
    if not os.path.exists(ca_key_path):
        key = paramiko.RSAKey.generate(bits=2048)
        key.write_private_key_file(ca_key_path)
        os.chmod(ca_key_path, 0o600)  # Set proper permissions for the private key

    # Generate an SSH certificate for the CA if it doesn't already exist
    if not os.path.exists(ca_cert_path):
        with open(ca_key_path, 'r') as key_file:
            private_key = paramiko.RSAKey.from_private_key(key_file)

            try:
                cert = paramiko.RSAKey.generate_cert(public_key=private_key.get_base64(),
                                                      private_key=private_key,
                                                      key_id="ca@mydomain.com",
                                                      valid_principals=["*"],
                                                      valid_after=None,
                                                      valid_before=None)
                cert.write_certificate_file(ca_cert_path)
            except SSHException as e:
                print(f"Error generating CA certificate: {e}")

def sign_ssh_key(ca_key_path, user_key_path, user_cert_path):
    # Load the CA private key and user public key
    with open(ca_key_path, 'r') as ca_key_file, open(user_key_path, 'r') as user_key_file:
        ca_private_key = paramiko.RSAKey.from_private_key(ca_key_file)
        user_public_key = paramiko.RSAKey.from_private_key(user_key_file)

        try:
            cert = paramiko.RSAKey.generate_cert(public_key=user_public_key.get_base64(),
                                                  private_key=ca_private_key,
                                                  key_id=f"user@{user_public_key.get_fingerprint()}",
                                                  valid_principals=[f"user@{user_public_key.get_fingerprint()}"],
                                                  valid_after=None,
                                                  valid_before=None)
            cert.write_certificate_file(user_cert_path)
        except SSHException as e:
            print(f"Error signing user key: {e}")

if __name__ == "__main__":
    # Paths for the CA key and certificate
    ca_key_path = "ca_key"
    ca_cert_path = "ca_cert"

    # Create the SSH CA if it doesn't exist
    create_ssh_ca(ca_key_path, ca_cert_path)

    # Example: Sign a user key with the CA
    user_key_path = "user_key.pub"
    user_cert_path = "user_cert.pub"

    # Sign the user key with the CA
    sign_ssh_key(ca_key_path, user_key_path, user_cert_path)
