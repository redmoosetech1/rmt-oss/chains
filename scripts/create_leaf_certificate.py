from cryptography import x509
from cryptography.hazmat.primitives import hashes, serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from datetime import datetime, timedelta
from cryptography.hazmat.primitives.serialization import Encoding, PrivateFormat, NoEncryption

def create_certificate(intermediate_cert, intermediate_key, common_name, is_server_cert=True):
    # Generate a private key for the certificate
    private_key = rsa.generate_private_key(
        public_exponent=65537,
        key_size=2048
    )

    # Create a certificate signing request (CSR) for the certificate
    csr = x509.CertificateSigningRequestBuilder().subject_name(
        x509.Name([
            x509.NameAttribute(x509.NameOID.COMMON_NAME, common_name),
        ])
    ).sign(private_key, hashes.SHA256())

    # Determine if it's a server certificate or a user certificate
    if is_server_cert:
        extended_key_usage = [x509.ExtendedKeyUsageOID.SERVER_AUTH]
    else:
        extended_key_usage = [x509.ExtendedKeyUsageOID.CLIENT_AUTH]

    # Sign the CSR with the intermediate CA's private key to create the certificate
    cert = x509.CertificateBuilder().subject_name(
        csr.subject
    ).issuer_name(
        intermediate_cert.subject
    ).public_key(
        csr.public_key()
    ).serial_number(
        x509.random_serial_number()
    ).not_valid_before(
        datetime.utcnow()
    ).not_valid_after(
        datetime.utcnow() + timedelta(days=365)  # Valid for 1 year
    ).add_extension(
        x509.ExtendedKeyUsage(extended_key_usage), critical=False,
    ).sign(intermediate_key, hashes.SHA256())

    # Save the private key and certificate to files
    with open(f"{common_name}_private_key.pem", "wb") as key_file:
        key_file.write(
            private_key.private_bytes(
                Encoding.PEM, PrivateFormat.TraditionalOpenSSL, NoEncryption()
            )
        )

    with open(f"{common_name}_certificate.pem", "wb") as cert_file:
        cert_file.write(cert.public_bytes(Encoding.PEM))

if __name__ == "__main__":
    # Load the intermediate CA's private key and certificate
    with open("intermediate_ca_private_key.pem", "rb") as intermediate_key_file:
        intermediate_key = serialization.load_pem_private_key(
            intermediate_key_file.read(), password=None
        )

    with open("intermediate_ca_certificate.pem", "rb") as intermediate_cert_file:
        intermediate_cert = x509.load_pem_x509_certificate(intermediate_cert_file.read())

    # Create a server certificate
    create_certificate(intermediate_cert, intermediate_key, "MyServer", is_server_cert=True)

    # Create a user certificate
    create_certificate(intermediate_cert, intermediate_key, "MyUser", is_server_cert=False)
