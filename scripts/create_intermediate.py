from cryptography import x509
from cryptography.hazmat.primitives import hashes, serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from datetime import datetime, timedelta
from cryptography.hazmat.primitives.serialization import Encoding, PrivateFormat, NoEncryption

def create_intermediate_ca(ca_cert, ca_key):
    # Generate a private key for the intermediate CA
    private_key = rsa.generate_private_key(
        public_exponent=65537,
        key_size=2048
    )

    # Create a certificate signing request (CSR) for the intermediate CA
    csr = x509.CertificateSigningRequestBuilder().subject_name(
        x509.Name([
            x509.NameAttribute(x509.NameOID.COMMON_NAME, u'Intermediate CA'),
        ])
    ).sign(private_key, hashes.SHA256())

    # Sign the CSR with the CA's private key to create the intermediate CA certificate
    intermediate_cert = x509.CertificateBuilder().subject_name(
        csr.subject
    ).issuer_name(
        ca_cert.subject
    ).public_key(
        csr.public_key()
    ).serial_number(
        x509.random_serial_number()
    ).not_valid_before(
        datetime.utcnow()
    ).not_valid_after(
        datetime.utcnow() + timedelta(days=3650)  # Valid for 10 years
    ).add_extension(
        x509.BasicConstraints(ca=True, path_length=None), critical=True,
    ).sign(ca_key, hashes.SHA256())

    # Save the private key and intermediate CA certificate to files
    with open("intermediate_ca_private_key.pem", "wb") as key_file:
        key_file.write(
            private_key.private_bytes(
                Encoding.PEM, PrivateFormat.TraditionalOpenSSL, NoEncryption()
            )
        )

    with open("intermediate_ca_certificate.pem", "wb") as cert_file:
        cert_file.write(intermediate_cert.public_bytes(Encoding.PEM))

if __name__ == "__main__":
    # Load the CA's private key and certificate
    with open("ca_private_key.pem", "rb") as ca_key_file:
        ca_key = serialization.load_pem_private_key(
            ca_key_file.read(), password=None
        )

    with open("ca_certificate.pem", "rb") as ca_cert_file:
        ca_cert = x509.load_pem_x509_certificate(ca_cert_file.read())

    # Create the intermediate CA
    create_intermediate_ca(ca_cert, ca_key)
