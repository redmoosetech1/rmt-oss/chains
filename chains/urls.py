"""
URL configuration for chains project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/5.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, re_path, include
from django.views.generic import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.conf import settings


class HomeView(LoginRequiredMixin, TemplateView):
    template_name = "home.html"
    login_url = settings.LOGIN_URL


urlpatterns = [
    path('api/', include('api.urls')),
    re_path("^accounts/.*$", TemplateView.as_view(template_name="accounts.html"), name="accounts"),
    re_path("^.*$", HomeView.as_view(), name="home"),
]
